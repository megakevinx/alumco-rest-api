﻿using AlumcoRestApi.ResourceModels;
using AlumcoRestApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace AlumcoRestApi.Services
{
    public class ServiceCallService : IDisposable
    {
        private AlumcoEntityDataModel db = new AlumcoEntityDataModel();

        public void Dispose()
        {
            db.Dispose();
        }

        public IEnumerable<ServiceCallResourceModel> GetAllForDate(string date)
        {
            DateTime searchDate = DateTime.Parse(date);

            var query = db.ServiceCalls
                .Where(sc => sc.AppointmentDate.HasValue && DbFunctions.TruncateTime(sc.AppointmentDate.Value) == searchDate)
                .Select(ConstructServiceCallResourceModel);

            return query.ToList();
        }

        private ServiceCallResourceModel ConstructServiceCallResourceModel(ServiceCall sc)
        {
            return new ServiceCallResourceModel()
            {
                ServiceCallId = sc.ServiceCallId,
                JobNumber = sc.Job.JobNumber,
                CustomerName = sc.Job.Customer.FirstName1 + " " + sc.Job.Customer.LastName1,
                Address = sc.Job.JobAddress,
                City = sc.Job.JobCity,
                State = sc.Job.JobState,
                Zip = sc.Job.JobZip,

                PhoneNumbers = sc.Job.Customer.CustomerPhoneNumbers.Select(ConstructCustomerPhoneNumberResourceModel),
                ServiceTechs = sc.ServiceCallTeches.Select(ConstructServiceCallTechResourceModel),
            };
        }

        private CustomerPhoneNumberResourceModel ConstructCustomerPhoneNumberResourceModel(CustomerPhoneNumber cpn)
        {
            return new CustomerPhoneNumberResourceModel()
            {
                Number = cpn.PhoneNumber,
                Extension = cpn.Extension,
                Type = cpn.PhoneNumberType.Name,
                IsPreferred = cpn.Preferred
            };
        }

        private ServiceCallTechResourceModel ConstructServiceCallTechResourceModel(ServiceCallTech sct)
        {
            if (sct.ContractorId.HasValue)
            {
                return new ServiceCallTechResourceModel()
                {
                    Name = sct.Contractor.FirstName + " " + sct.Contractor.LastName,
                    Email = sct.Contractor.Email,
                    HomePhone = sct.Contractor.HomePhone,
                    MobilePhone = sct.Contractor.MobilePhone
                };
            }
            else if (sct.ServiceTechId.HasValue)
            {
                return new ServiceCallTechResourceModel()
                {
                    Name = sct.User.FirstName + " " + sct.User.LastName,
                    Email = sct.User.aspnet_Users.aspnet_Membership.Email,
                    HomePhone = sct.User.HomeNumber,
                    MobilePhone = sct.User.MobileNumber
                };
            }
            else
            {
                return null;
            }
        }
    }
}