﻿using AlumcoRestApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlumcoRestApi.Services
{
    public class DistributionListService : IDisposable
    {
        private AlumcoEntityDataModel db = new AlumcoEntityDataModel();

        public void Dispose()
        {
            db.Dispose();
        }

        public IEnumerable<string> GetRecipientsForList(string distributionListName)
        {
            var listRecipients = new List<string>();

            listRecipients = db.DistributionLists
                .Single(dl => dl.Name == distributionListName)
                .DistributionListRecipients
                .Select(dlr => dlr.Email).ToList();

            return listRecipients;
        }
    }
}