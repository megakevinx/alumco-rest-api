﻿using AlumcoRestApi.ResourceModels;
using AlumcoRestApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlumcoRestApi.Services
{
    public class LeadService : IDisposable
    {
        private AlumcoEntityDataModel db = new AlumcoEntityDataModel();

        public void Dispose()
        {
            db.Dispose();
        }

        public IEnumerable<LeadResourceModel> GetAll(int page, int count)
        {
            var leadsToReturn = db.Leads
                .OrderByDescending(l => l.DateCreated)
                .Skip(page * count)
                .Take(count)
                .Select(ConstructLeadResourceModel);

            return leadsToReturn;
        }

        public LeadResourceModel GetById(Guid id)
        {
            var leadToReturn = db.Leads

                .Include("User").Include("LeadStatu").Include("LeadSource")
                .Include("LeadPhoneNumbers.PhoneNumberType")
                .Include("LeadServices.Service")

                .Where(l => l.Id == id)
                .Select(ConstructLeadResourceModel)
                .FirstOrDefault();

            return leadToReturn;
        }

        private LeadResourceModel ConstructLeadResourceModel(Lead lead)
        {
            return new LeadResourceModel()
            {
                Id = lead.Id,
                FirstName = lead.FirstName,
                FirstName2 = lead.FirstName2,
                LastName = lead.LastName,
                LastName2 = lead.LastName2,
                SalesPersonId = lead.SalesPersonId,
                Email = lead.Email,
                Address = lead.Address,
                ZipCode = lead.ZipCode,
                City = lead.City,
                State = lead.State,
                Amount = lead.Amount,
                DateSold = lead.DateSold,
                PrefixName = lead.PrefixName,
                PrefixName2 = lead.PrefixName2,
                Description = lead.Description,
                ReferralName = lead.ReferralName,
                SourceOther = lead.SourceOther,
                StatusId = lead.StatusId,
                SourceId = lead.SourceId,
                DateCreated = lead.DateCreated,
                DateModified = lead.DateModified,

                SalesPersonFullName = lead.User.FirstName + " " + lead.User.LastName,
                LeadStatusName = lead.LeadStatu.Name,
                LeadSourceName = (lead.SourceId.HasValue ? lead.LeadSource.Name : ""),

                LeadPhoneNumbers = lead.LeadPhoneNumbers.Select(lpn => new LeadPhoneNumberResourceModel()
                {
                    Id = lpn.Id,
                    LeadID = lpn.LeadID,
                    PhoneNumber = lpn.PhoneNumber,
                    PhoneNumberTypeId = lpn.PhoneNumberTypeId,
                    Extension = lpn.Extension,
                    Preferred = lpn.Preferred,

                    PhoneNumberTypeName = lpn.PhoneNumberType.Name
                }),

                LeadServices = lead.LeadServices.Select(ls => new LeadServiceResourceModel()
                {
                    Id = ls.Id,
                    ServiceId = ls.ServiceId,
                    LeadId = ls.LeadId,

                    ServiceName = ls.Service.Name
                })
            };
        }

        public LeadResourceModel Create(LeadResourceModel lead)
        {
            Lead leadToSave = new Lead()
            {
                FirstName = lead.FirstName,
                FirstName2 = lead.FirstName2,
                LastName = lead.LastName,
                LastName2 = lead.LastName2,
                SalesPersonId = lead.SalesPersonId,
                Email = lead.Email,
                Address = lead.Address,
                ZipCode = lead.ZipCode,
                City = lead.City,
                State = lead.State,
                Amount = lead.Amount,
                DateSold = lead.DateSold,
                PrefixName = lead.PrefixName,
                PrefixName2 = lead.PrefixName2,
                Description = lead.Description,
                ReferralName = lead.ReferralName,
                SourceOther = lead.SourceOther,
                StatusId = lead.StatusId,
                SourceId = lead.SourceId,
                DateCreated = DateTime.Now,
                DateModified = DateTime.Now,
            };

            if (!String.IsNullOrWhiteSpace(lead.LeadStatusName))
            {
                leadToSave.StatusId = db.LeadStatus.Single(ls => ls.Name == lead.LeadStatusName).Id;
            }

            if (!String.IsNullOrWhiteSpace(lead.LeadSourceName))
            {
                leadToSave.SourceId = db.LeadSources.Single(ls => ls.Name == lead.LeadSourceName).Id;
            }

            if (lead.LeadPhoneNumbers != null)
            {
                foreach (var leadPhoneNumber in lead.LeadPhoneNumbers)
                {
                    var leadPhoneNumberToSave = new LeadPhoneNumber()
                    {
                        PhoneNumber = leadPhoneNumber.PhoneNumber,
                        PhoneNumberTypeId = leadPhoneNumber.PhoneNumberTypeId,
                        Extension = leadPhoneNumber.Extension,
                        Preferred = leadPhoneNumber.Preferred
                    };

                    if (!String.IsNullOrWhiteSpace(leadPhoneNumber.PhoneNumberTypeName))
                    {
                        leadPhoneNumberToSave.PhoneNumberTypeId =
                            db.PhoneNumberTypes.Single(pnt => pnt.Name == leadPhoneNumber.PhoneNumberTypeName).Id;
                    }

                    leadToSave.LeadPhoneNumbers.Add(leadPhoneNumberToSave);
                }
            }

            if (lead.LeadServices != null)
            {
                foreach (var leadService in lead.LeadServices)
                {
                    var leadServiceToSave = new Models.LeadService()
                    {
                        ServiceId = leadService.ServiceId
                    };

                    if (!String.IsNullOrWhiteSpace(leadService.ServiceName))
                    {
                        leadServiceToSave.ServiceId =
                            db.Services.Single(s => s.Name == leadService.ServiceName).Id;
                    }

                    leadToSave.LeadServices.Add(leadServiceToSave);
                }
            }

            db.Leads.Add(leadToSave);

            db.SaveChanges();

            return GetById(leadToSave.Id);
        }

        public bool Exists(Guid id)
        {
            return db.Leads.Count(e => e.Id == id) > 0;
        }
    }
}