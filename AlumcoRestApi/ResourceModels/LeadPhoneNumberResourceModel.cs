﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlumcoRestApi.ResourceModels
{
    public class LeadPhoneNumberResourceModel
    {
        public Guid Id { get; set; }
        public Guid LeadID { get; set; }
        public string PhoneNumber { get; set; }
        public Guid PhoneNumberTypeId { get; set; }
        public string Extension { get; set; }
        public bool Preferred { get; set; }

        public string PhoneNumberTypeName { get; set; }
    }
}