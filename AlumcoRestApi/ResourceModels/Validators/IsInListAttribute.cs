﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AlumcoRestApi.ResourceModels.Validators
{
    public abstract class IsInListOfStringAttribute : ValidationAttribute
    {
        protected abstract List<string> GetListToValidateAgainst();

        public override bool IsValid(object value)
        {
            return this.GetListToValidateAgainst().Contains(value.ToString());
        }
    }
}
