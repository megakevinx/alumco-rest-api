﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace AlumcoRestApi.ResourceModels.Validators
{
    public sealed class TitleOfCourtesyAttribute : IsInListOfStringAttribute
    {
        protected override List<string> GetListToValidateAgainst()
        {
            return ConfigurationManager.AppSettings["TitlesOfCourtesy"].ToString().Split(',').ToList();
        }
    }
}
