﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace AlumcoRestApi.ResourceModels.Validators
{
    public sealed class SupportedStateAttribute : IsInListOfStringAttribute
    {
        protected override List<string> GetListToValidateAgainst()
        {
            return ConfigurationManager.AppSettings["SupportedStates"].ToString().Split(',').ToList();
        }
    }
}
