﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AlumcoRestApi.ResourceModels.Validators;

namespace AlumcoRestApi.ResourceModels
{
    public class LeadResourceModel
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string FirstName2 { get; set; }
        public string LastName2 { get; set; }
        public string LastName { get; set; }
        public Guid SalesPersonId { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }

        [SupportedState(ErrorMessage = "The specified State is not valid. It must be a supported state.")]
        public string State { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? DateSold { get; set; }

        [TitleOfCourtesy(ErrorMessage = "The specified PrefixName is not valid. It must be a valid title of courtesy.")]
        public string PrefixName { get; set; }
        [TitleOfCourtesy(ErrorMessage = "The specified PrefixName2 is not valid. It must be a valid title of courtesy.")]
        public string PrefixName2 { get; set; }
        public string Description { get; set; }
        public string ReferralName { get; set; }
        public string SourceOther { get; set; }
        public Guid StatusId { get; set; }
        public Guid? SourceId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }

        public IEnumerable<LeadPhoneNumberResourceModel> LeadPhoneNumbers { get; set; }
        public IEnumerable<LeadServiceResourceModel> LeadServices { get; set; }

        public string LeadStatusName { get; set; }
        public string LeadSourceName { get; set; }
        public string SalesPersonFullName { get; set; }
    }
}