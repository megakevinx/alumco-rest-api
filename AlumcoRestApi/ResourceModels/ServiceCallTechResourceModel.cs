﻿namespace AlumcoRestApi.ResourceModels
{
    public class ServiceCallTechResourceModel
    {
        public ServiceCallTechResourceModel()
        {
        }

        public string Name { get; set; }
        public string Email { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
    }
}
