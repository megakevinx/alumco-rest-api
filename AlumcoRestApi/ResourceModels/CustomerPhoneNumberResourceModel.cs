﻿namespace AlumcoRestApi.ResourceModels
{
    public class CustomerPhoneNumberResourceModel
    {
        public CustomerPhoneNumberResourceModel()
        {
        }

        public string Number { get; set; }
        public string Extension { get; set; }
        public string Type { get; set; }
        public bool IsPreferred { get; set; }
    }
}