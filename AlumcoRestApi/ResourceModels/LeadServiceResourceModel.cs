﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlumcoRestApi.ResourceModels
{
    public class LeadServiceResourceModel
    {
        public Guid Id { get; set; }
        public Guid ServiceId { get; set; }
        public Guid LeadId { get; set; }

        public string ServiceName { get; set; }
    }
}