﻿using System.Collections.Generic;

namespace AlumcoRestApi.ResourceModels
{
    public class ServiceCallResourceModel
    {
        public ServiceCallResourceModel()
        {
        }

        public int ServiceCallId { get; set; }
        public int JobNumber { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public IEnumerable<CustomerPhoneNumberResourceModel> PhoneNumbers { get; set; }
        public IEnumerable<ServiceCallTechResourceModel> ServiceTechs { get; set; }
    }
}