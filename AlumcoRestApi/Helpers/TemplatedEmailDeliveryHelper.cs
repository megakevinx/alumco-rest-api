﻿using AlumcoRestApi.Services;
using RazorEngine;
using RazorEngine.Templating;
using System.Collections.Generic;
using System.Net.Mail;
using System.Web.Hosting;

namespace AlumcoRestApi.Helpers
{
    public class TemplatedEmailDeliveryHelper
    {
        DistributionListService service;

        public TemplatedEmailDeliveryHelper(DistributionListService service)
        {
            this.service = service;
        }

        public void SendToDistributionList<T>(string templatePath, T viewModel, string from, string toDistributionList, string subject)
        {
            string template = getTemplate(templatePath);
            var recipients = service.GetRecipientsForList(toDistributionList);

            var body = Engine.Razor.RunCompile(template, templatePath, typeof(T), viewModel);

            SendEmail(from, recipients, subject, body);
        }

        public void SendToSingleRecipient(string contentPath, string from, string to, string subject)
        {
            string body = getTemplate(contentPath);
            string[] recipients = { to };

            SendEmail(from, recipients, subject, body);
        }

        private string getTemplate(string templatePath)
        {
            return System.IO.File.ReadAllText(
                HostingEnvironment.MapPath(templatePath)
            );
        }

        private void SendEmail(string from, IEnumerable<string> recipients, string subject, string result)
        {
            var msg = new MailMessage()
            {
                From = new MailAddress(from),
                Subject = subject,
                IsBodyHtml = true,
                Body = result
            };

            foreach (var recipient in recipients)
            {
                msg.To.Add(recipient);
            }
            
            var mail = new SmtpClient();
            mail.Send(msg);
        }
    }
}