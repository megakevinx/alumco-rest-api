namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ShareDocument")]
    public partial class ShareDocument
    {
        public Guid Id { get; set; }

        public Guid ShareId { get; set; }

        public Guid DocumentId { get; set; }

        public virtual JobDocument JobDocument { get; set; }

        public virtual Share Share { get; set; }
    }
}
