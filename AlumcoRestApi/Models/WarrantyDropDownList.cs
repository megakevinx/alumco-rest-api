namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("WarrantyDropDownList")]
    public partial class WarrantyDropDownList
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public WarrantyDropDownList()
        {
            WarrantyDropDownXRefs = new HashSet<WarrantyDropDownXRef>();
            WarrantyValuesXRefs = new HashSet<WarrantyValuesXRef>();
        }

        public Guid Id { get; set; }

        public Guid DropDownId { get; set; }

        [Required]
        [StringLength(100)]
        public string ItemName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WarrantyDropDownXRef> WarrantyDropDownXRefs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WarrantyValuesXRef> WarrantyValuesXRefs { get; set; }
    }
}
