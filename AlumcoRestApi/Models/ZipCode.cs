namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ZipCode")]
    public partial class ZipCode
    {
        public Guid Id { get; set; }

        [Column("ZipCode")]
        [Required]
        [StringLength(50)]
        public string ZipCode1 { get; set; }

        [Required]
        [StringLength(100)]
        public string City { get; set; }

        [Required]
        [StringLength(50)]
        public string State { get; set; }
    }
}
