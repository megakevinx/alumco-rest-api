namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Warranty")]
    public partial class Warranty
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Warranty()
        {
            WarrantyValuesXRefs = new HashSet<WarrantyValuesXRef>();
        }

        public Guid Id { get; set; }

        public Guid JobId { get; set; }

        public Guid WarrantyItemId { get; set; }

        public bool Singular { get; set; }

        public bool Downspout { get; set; }

        public bool ColorPlus { get; set; }

        [StringLength(100)]
        public string ColorPlusYears { get; set; }

        public virtual Job Job { get; set; }

        public virtual WarrantyItem WarrantyItem { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WarrantyValuesXRef> WarrantyValuesXRefs { get; set; }
    }
}
