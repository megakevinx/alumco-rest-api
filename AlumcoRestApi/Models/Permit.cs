namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Permit")]
    public partial class Permit
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Permit()
        {
            PermitDocuments = new HashSet<PermitDocument>();
        }

        public Guid Id { get; set; }

        public Guid JobId { get; set; }

        public Guid? PermitStatusId { get; set; }

        [Required]
        [StringLength(50)]
        public string County { get; set; }

        [Required]
        [StringLength(50)]
        public string PermitNumber { get; set; }

        [Required]
        [StringLength(50)]
        public string InspectorNumber { get; set; }

        [StringLength(50)]
        public string InspectorFirstName { get; set; }

        [StringLength(50)]
        public string InspectorLastName { get; set; }

        public Guid? ProjectManagerId { get; set; }

        [Required]
        public string Notes { get; set; }

        public virtual Job Job { get; set; }

        public virtual PermitStatu PermitStatu { get; set; }

        public virtual User User { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PermitDocument> PermitDocuments { get; set; }
    }
}
