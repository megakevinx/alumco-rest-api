namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("VendorService")]
    public partial class VendorService
    {
        public Guid Id { get; set; }

        public Guid ServiceId { get; set; }

        public Guid VendorId { get; set; }

        public virtual Service Service { get; set; }

        public virtual Vendor Vendor { get; set; }
    }
}
