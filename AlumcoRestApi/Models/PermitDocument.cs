namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PermitDocument")]
    public partial class PermitDocument
    {
        public Guid Id { get; set; }

        public Guid PermitId { get; set; }

        [Required]
        [StringLength(256)]
        public string Path { get; set; }

        public virtual Permit Permit { get; set; }
    }
}
