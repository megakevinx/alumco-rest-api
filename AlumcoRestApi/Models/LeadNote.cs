namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LeadNote")]
    public partial class LeadNote
    {
        public Guid Id { get; set; }

        public Guid LeadId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public DateTime DateCreated { get; set; }

        [Required]
        [StringLength(256)]
        public string CreatedBy { get; set; }

        [Required]
        public string Description { get; set; }

        public DateTime DateModified { get; set; }

        [Required]
        [StringLength(256)]
        public string ModifiedBy { get; set; }

        public virtual Lead Lead { get; set; }
    }
}
