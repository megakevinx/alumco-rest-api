namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Job")]
    public partial class Job
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Job()
        {
            GutterWorkOrders = new HashSet<GutterWorkOrder>();
            JobSigns = new HashSet<JobSign>();
            JobContractors = new HashSet<JobContractor>();
            JobDocuments = new HashSet<JobDocument>();
            JobFlags = new HashSet<JobFlag>();
            JobLogs = new HashSet<JobLog>();
            JobMaterialLogouts = new HashSet<JobMaterialLogout>();
            JobNotes = new HashSet<JobNote>();
            JobOldCustomers = new HashSet<JobOldCustomer>();
            JobPriceAdjustments = new HashSet<JobPriceAdjustment>();
            JobProblems = new HashSet<JobProblem>();
            JobRRPEmails = new HashSet<JobRRPEmail>();
            Permits = new HashSet<Permit>();
            ServiceCalls = new HashSet<ServiceCall>();
            ServiceComments1 = new HashSet<ServiceComment>();
            Shares = new HashSet<Share>();
            UserFavoriteJobs = new HashSet<UserFavoriteJob>();
            Warranties = new HashSet<Warranty>();
            WorkOrders = new HashSet<WorkOrder>();
        }

        public Guid Id { get; set; }

        public Guid CustomerId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int JobNumber { get; set; }

        public DateTime? ContractDate { get; set; }

        public Guid? SalesPersonId { get; set; }

        public Guid JobStatusId { get; set; }

        public decimal? Price { get; set; }

        public bool SpecialNeeds { get; set; }

        [Required]
        public string SpecialNeedsNotes { get; set; }

        public Guid TermId { get; set; }

        [Required]
        public string TermsDescription { get; set; }

        public string Summary { get; set; }

        [Required]
        [StringLength(55)]
        public string JobAddress { get; set; }

        [Required]
        [StringLength(20)]
        public string JobCity { get; set; }

        [Required]
        [StringLength(2)]
        public string JobState { get; set; }

        [Required]
        [StringLength(5)]
        public string JobZip { get; set; }

        [Required]
        public string JobAddressPreview { get; set; }

        public bool PermitRequired { get; set; }

        public DateTime? SignOutDate { get; set; }

        public DateTime? SignInDate { get; set; }

        public bool Cardboard { get; set; }

        public DateTime? WorkStartDate { get; set; }

        public DateTime? WorkEndDate { get; set; }

        public DateTime? WorkPaidDate { get; set; }

        public decimal? CommissionPaid { get; set; }

        public Guid? LeadSourceId { get; set; }

        [Required]
        public string Comments { get; set; }

        public Guid? LeadId { get; set; }

        public bool GutterWork { get; set; }

        public bool NonGutterWork { get; set; }

        public int? OldJobId { get; set; }

        public string InvoiceComments { get; set; }

        public DateTime? CollectionDate { get; set; }

        public Guid? ProjectManagerId { get; set; }

        public Guid? SalesPersonId2 { get; set; }

        public string ServiceComments { get; set; }

        public DateTime? GuildQualitySurvey { get; set; }

        public Guid? CollectedBy { get; set; }

        public DateTime? CollectedByDate { get; set; }

        [StringLength(100)]
        public string CollectedByUser { get; set; }

        public bool? Punchlist { get; set; }

        [Required]
        public string OtherContractor { get; set; }

        public decimal? Latitude { get; set; }

        public decimal? Longitude { get; set; }

        public DateTime? GeocodeDate { get; set; }

        public bool SendEmailOfRRP { get; set; }

        public bool HaveSignedHardCopy { get; set; }

        public bool HaveReturnedCopy { get; set; }

        public DateTime? SendEmailOfRRPDate { get; set; }

        public DateTime? HaveSignedHardCopyDate { get; set; }

        public DateTime? HaveReturnedCopyDate { get; set; }

        [StringLength(100)]
        public string SendEmailOfRRPBy { get; set; }

        [StringLength(100)]
        public string HaveSignedHardCopyBy { get; set; }

        [StringLength(100)]
        public string HaveReturnedCopyBy { get; set; }

        public bool? HouseBuiltPre1978 { get; set; }

        public bool PreLeadRequirement { get; set; }

        public virtual Customer Customer { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GutterWorkOrder> GutterWorkOrders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobSign> JobSigns { get; set; }

        public virtual JobStatu JobStatu { get; set; }

        public virtual Lead Lead { get; set; }

        public virtual LeadSource LeadSource { get; set; }

        public virtual Term Term { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        public virtual User User2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobContractor> JobContractors { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobDocument> JobDocuments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobFlag> JobFlags { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobLog> JobLogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobMaterialLogout> JobMaterialLogouts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobNote> JobNotes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobOldCustomer> JobOldCustomers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobPriceAdjustment> JobPriceAdjustments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobProblem> JobProblems { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobRRPEmail> JobRRPEmails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Permit> Permits { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ServiceCall> ServiceCalls { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ServiceComment> ServiceComments1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Share> Shares { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserFavoriteJob> UserFavoriteJobs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Warranty> Warranties { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WorkOrder> WorkOrders { get; set; }
    }
}
