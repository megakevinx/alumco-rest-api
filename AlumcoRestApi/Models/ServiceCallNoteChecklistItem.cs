namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ServiceCallNoteChecklistItem")]
    public partial class ServiceCallNoteChecklistItem
    {
        public int Id { get; set; }

        public Guid ServiceCallId { get; set; }

        public int ServiceCallNoteChecklistItemTypeId { get; set; }

        public bool IsChecked { get; set; }

        public string Notes { get; set; }

        public virtual ServiceCall ServiceCall { get; set; }

        public virtual ServiceCallNoteChecklistItemType ServiceCallNoteChecklistItemType { get; set; }
    }
}
