namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserFavoriteJob")]
    public partial class UserFavoriteJob
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public Guid JobId { get; set; }

        public virtual Job Job { get; set; }

        public virtual User User { get; set; }
    }
}
