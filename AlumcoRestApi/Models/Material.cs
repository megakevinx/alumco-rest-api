namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Material")]
    public partial class Material
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Material()
        {
            WorkOrderMaterials = new HashSet<WorkOrderMaterial>();
        }

        public Guid Id { get; set; }

        [Required]
        [StringLength(20)]
        public string UnitType { get; set; }

        [Column("Material")]
        [Required]
        [StringLength(150)]
        public string Material1 { get; set; }

        public int SortOrder { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WorkOrderMaterial> WorkOrderMaterials { get; set; }
    }
}
