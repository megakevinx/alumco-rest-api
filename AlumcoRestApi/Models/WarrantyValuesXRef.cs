namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("WarrantyValuesXRef")]
    public partial class WarrantyValuesXRef
    {
        public Guid Id { get; set; }

        public Guid WarrantyId { get; set; }

        public Guid WarrantyDropDownValueId { get; set; }

        public virtual Warranty Warranty { get; set; }

        public virtual WarrantyDropDownList WarrantyDropDownList { get; set; }
    }
}
