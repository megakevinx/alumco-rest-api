namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("WarrantyItem")]
    public partial class WarrantyItem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public WarrantyItem()
        {
            Warranties = new HashSet<Warranty>();
            WarrantyDropDownXRefs = new HashSet<WarrantyDropDownXRef>();
        }

        public Guid Id { get; set; }

        [Required]
        [StringLength(100)]
        public string WarrantyItemName { get; set; }

        public bool HasPluralOption { get; set; }

        public bool HasDownspoutOption { get; set; }

        [StringLength(255)]
        public string WarrantyLineText { get; set; }

        public bool HasColorPlusOption { get; set; }

        public bool Enabled { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Warranty> Warranties { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WarrantyDropDownXRef> WarrantyDropDownXRefs { get; set; }
    }
}
