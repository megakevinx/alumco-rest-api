namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobRRPEmail")]
    public partial class JobRRPEmail
    {
        public Guid Id { get; set; }

        public Guid JobId { get; set; }

        public DateTime DateSent { get; set; }

        [Required]
        [StringLength(255)]
        public string SentBy { get; set; }

        public virtual Job Job { get; set; }
    }
}
