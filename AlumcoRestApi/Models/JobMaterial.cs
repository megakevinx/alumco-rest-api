namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobMaterial")]
    public partial class JobMaterial
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public JobMaterial()
        {
            JobMaterialItems = new HashSet<JobMaterialItem>();
        }

        public Guid Id { get; set; }

        [Required]
        [StringLength(150)]
        public string Material { get; set; }

        public bool SpecialOrderItem { get; set; }

        public bool StockItem { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobMaterialItem> JobMaterialItems { get; set; }
    }
}
