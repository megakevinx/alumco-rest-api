namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobPriceAdjustment")]
    public partial class JobPriceAdjustment
    {
        public Guid Id { get; set; }

        public Guid JobId { get; set; }

        public decimal Adjustment { get; set; }

        [Required]
        public string Description { get; set; }

        public DateTime DateCreated { get; set; }

        [Required]
        [StringLength(100)]
        public string CreatedBy { get; set; }

        public bool Active { get; set; }

        public DateTime? InactiveDate { get; set; }

        [StringLength(100)]
        public string InactiveBy { get; set; }

        public virtual Job Job { get; set; }
    }
}
