namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobMaterialItem")]
    public partial class JobMaterialItem
    {
        public Guid Id { get; set; }

        public int Quantity { get; set; }

        [Required]
        [StringLength(100)]
        public string ManufacturerNumber { get; set; }

        [Required]
        public string Notes { get; set; }

        public Guid JobMaterialId { get; set; }

        public Guid JobMaterialLogoutId { get; set; }

        public virtual JobMaterial JobMaterial { get; set; }

        public virtual JobMaterialLogout JobMaterialLogout { get; set; }
    }
}
