namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LeadService")]
    public partial class LeadService
    {
        //public Guid Id { get; set; }

        public Guid ServiceId { get; set; }

        public Guid LeadId { get; set; }

        public virtual Lead Lead { get; set; }

        public virtual Service Service { get; set; }
    }
}
