namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Lead")]
    public partial class Lead
    {
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        //public Lead()
        //{
        //    Jobs = new HashSet<Job>();
        //    LeadDocuments = new HashSet<LeadDocument>();
        //    LeadNotes = new HashSet<LeadNote>();
        //    LeadProblems = new HashSet<LeadProblem>();
        //    LeadServices = new HashSet<LeadService>();
        //    LeadPhoneNumbers = new HashSet<LeadPhoneNumber>();
        //}

        //public Guid Id { get; set; }

        public Guid SalesPersonId { get; set; }

        [Required]
        [StringLength(50)]
        public string PrefixName { get; set; }

        [Required]
        [StringLength(50)]
        public string PrefixName2 { get; set; }

        [Required]
        [StringLength(100)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100)]
        public string LastName { get; set; }

        [Required]
        [StringLength(100)]
        public string FirstName2 { get; set; }

        [Required]
        [StringLength(100)]
        public string LastName2 { get; set; }

        [Required]
        [StringLength(100)]
        public string Address { get; set; }

        [Required]
        [StringLength(100)]
        public string City { get; set; }

        [Required]
        [StringLength(50)]
        public string State { get; set; }

        [Required]
        [StringLength(50)]
        public string ZipCode { get; set; }

        [Required]
        [StringLength(100)]
        public string Email { get; set; }

        [Required]
        [StringLength(100)]
        public string ReferralName { get; set; }

        [Required]
        public string Description { get; set; }

        public Guid StatusId { get; set; }

        public DateTime? DateSold { get; set; }

        public decimal? Amount { get; set; }

        public Guid? SourceId { get; set; }

        [StringLength(255)]
        public string SourceOther { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Job> Jobs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LeadDocument> LeadDocuments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LeadNote> LeadNotes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LeadProblem> LeadProblems { get; set; }

        public virtual LeadSource LeadSource { get; set; }

        public virtual LeadStatu LeadStatu { get; set; }

        public virtual User User { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LeadService> LeadServices { get; set; }
    }
}
