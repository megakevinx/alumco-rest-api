namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ServiceCallTech")]
    public partial class ServiceCallTech
    {
        public Guid Id { get; set; }

        public Guid ServiceCallId { get; set; }

        public Guid? ServiceTechId { get; set; }

        public Guid? ContractorId { get; set; }

        public virtual Contractor Contractor { get; set; }

        public virtual ServiceCall ServiceCall { get; set; }

        public virtual User User { get; set; }
    }
}
