namespace AlumcoRestApi.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class AlumcoEntityDataModel : DbContext
    {
        public AlumcoEntityDataModel()
            : base("name=AlumcoEntityDataModel")
        {
        }

        public virtual DbSet<aspnet_Applications> aspnet_Applications { get; set; }
        public virtual DbSet<aspnet_Membership> aspnet_Membership { get; set; }
        public virtual DbSet<aspnet_Paths> aspnet_Paths { get; set; }
        public virtual DbSet<aspnet_PersonalizationAllUsers> aspnet_PersonalizationAllUsers { get; set; }
        public virtual DbSet<aspnet_PersonalizationPerUser> aspnet_PersonalizationPerUser { get; set; }
        public virtual DbSet<aspnet_Profile> aspnet_Profile { get; set; }
        public virtual DbSet<aspnet_Roles> aspnet_Roles { get; set; }
        public virtual DbSet<aspnet_SchemaVersions> aspnet_SchemaVersions { get; set; }
        public virtual DbSet<aspnet_Users> aspnet_Users { get; set; }
        public virtual DbSet<aspnet_WebEvent_Events> aspnet_WebEvent_Events { get; set; }
        public virtual DbSet<Contractor> Contractors { get; set; }
        public virtual DbSet<ContractorDocument> ContractorDocuments { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<CustomerPhoneNumber> CustomerPhoneNumbers { get; set; }
        public virtual DbSet<DistributionList> DistributionLists { get; set; }
        public virtual DbSet<DistributionListRecipient> DistributionListRecipients { get; set; }
        public virtual DbSet<Document> Documents { get; set; }
        public virtual DbSet<DocumentType> DocumentTypes { get; set; }
        public virtual DbSet<GutterWorkOrder> GutterWorkOrders { get; set; }
        public virtual DbSet<Job> Jobs { get; set; }
        public virtual DbSet<JobContractor> JobContractors { get; set; }
        public virtual DbSet<JobDocument> JobDocuments { get; set; }
        public virtual DbSet<JobFlag> JobFlags { get; set; }
        public virtual DbSet<JobLog> JobLogs { get; set; }
        public virtual DbSet<JobMaterial> JobMaterials { get; set; }
        public virtual DbSet<JobMaterialItem> JobMaterialItems { get; set; }
        public virtual DbSet<JobMaterialLogout> JobMaterialLogouts { get; set; }
        public virtual DbSet<JobNote> JobNotes { get; set; }
        public virtual DbSet<JobOldCustomer> JobOldCustomers { get; set; }
        public virtual DbSet<JobPriceAdjustment> JobPriceAdjustments { get; set; }
        public virtual DbSet<JobProblem> JobProblems { get; set; }
        public virtual DbSet<JobRRPEmail> JobRRPEmails { get; set; }
        public virtual DbSet<JobSign> JobSigns { get; set; }
        public virtual DbSet<JobSignStatu> JobSignStatus { get; set; }
        public virtual DbSet<JobStatu> JobStatus { get; set; }
        public virtual DbSet<Lead> Leads { get; set; }
        public virtual DbSet<LeadDocument> LeadDocuments { get; set; }
        public virtual DbSet<LeadNote> LeadNotes { get; set; }
        public virtual DbSet<LeadPhoneNumber> LeadPhoneNumbers { get; set; }
        public virtual DbSet<LeadProblem> LeadProblems { get; set; }
        public virtual DbSet<LeadService> LeadServices { get; set; }
        public virtual DbSet<LeadSource> LeadSources { get; set; }
        public virtual DbSet<LeadStatu> LeadStatus { get; set; }
        public virtual DbSet<Material> Materials { get; set; }
        public virtual DbSet<MobileProvider> MobileProviders { get; set; }
        public virtual DbSet<Permit> Permits { get; set; }
        public virtual DbSet<PermitDocument> PermitDocuments { get; set; }
        public virtual DbSet<PermitStatu> PermitStatus { get; set; }
        public virtual DbSet<PhoneNumberType> PhoneNumberTypes { get; set; }
        public virtual DbSet<Position> Positions { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<ServiceCall> ServiceCalls { get; set; }
        public virtual DbSet<ServiceCallNoteChecklistItem> ServiceCallNoteChecklistItems { get; set; }
        public virtual DbSet<ServiceCallNoteChecklistItemType> ServiceCallNoteChecklistItemTypes { get; set; }
        public virtual DbSet<ServiceCallTech> ServiceCallTeches { get; set; }
        public virtual DbSet<ServiceComment> ServiceComments { get; set; }
        public virtual DbSet<ServiceDocument> ServiceDocuments { get; set; }
        public virtual DbSet<ServiceLog> ServiceLogs { get; set; }
        public virtual DbSet<ServiceProblem> ServiceProblems { get; set; }
        public virtual DbSet<ServiceTechNote> ServiceTechNotes { get; set; }
        public virtual DbSet<Share> Shares { get; set; }
        public virtual DbSet<ShareDocument> ShareDocuments { get; set; }
        public virtual DbSet<StockInventory> StockInventories { get; set; }
        public virtual DbSet<Supplier> Suppliers { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<Term> Terms { get; set; }
        public virtual DbSet<TypeOfService> TypeOfServices { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserFavoriteJob> UserFavoriteJobs { get; set; }
        public virtual DbSet<Vendor> Vendors { get; set; }
        public virtual DbSet<VendorService> VendorServices { get; set; }
        public virtual DbSet<Warranty> Warranties { get; set; }
        public virtual DbSet<WarrantyDropDownList> WarrantyDropDownLists { get; set; }
        public virtual DbSet<WarrantyDropDownXRef> WarrantyDropDownXRefs { get; set; }
        public virtual DbSet<WarrantyItem> WarrantyItems { get; set; }
        public virtual DbSet<WarrantyValuesXRef> WarrantyValuesXRefs { get; set; }
        public virtual DbSet<WorkOrder> WorkOrders { get; set; }
        public virtual DbSet<WorkOrderMaterial> WorkOrderMaterials { get; set; }
        public virtual DbSet<ZipCode> ZipCodes { get; set; }
        public virtual DbSet<DocuSign> DocuSigns { get; set; }
        public virtual DbSet<OldJobsPhoneNumber> OldJobsPhoneNumbers { get; set; }
        public virtual DbSet<PasswordHistory> PasswordHistories { get; set; }
    }
}
