namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DistributionListRecipient")]
    public partial class DistributionListRecipient
    {
        public int Id { get; set; }

        public int DistributionListId { get; set; }

        [Required]
        [StringLength(100)]
        public string Email { get; set; }

        public virtual DistributionList DistributionList { get; set; }
    }
}
