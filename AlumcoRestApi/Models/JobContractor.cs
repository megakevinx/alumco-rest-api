namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobContractor")]
    public partial class JobContractor
    {
        public Guid Id { get; set; }

        public Guid JobId { get; set; }

        public Guid? ContractorId { get; set; }

        public Guid? ServiceTechId { get; set; }

        public virtual Contractor Contractor { get; set; }

        public virtual Job Job { get; set; }

        public virtual User User { get; set; }
    }
}
