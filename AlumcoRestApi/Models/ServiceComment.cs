namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ServiceComment")]
    public partial class ServiceComment
    {
        public Guid Id { get; set; }

        public Guid JobId { get; set; }

        [Required]
        public string Notes { get; set; }

        public DateTime DateCreated { get; set; }

        [Required]
        [StringLength(256)]
        public string CreatedBy { get; set; }

        public virtual Job Job { get; set; }
    }
}
