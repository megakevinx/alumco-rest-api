namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class OldJobsPhoneNumber
    {
        [Key]
        public int CustomerPhoneID { get; set; }

        public int? JobID { get; set; }

        [StringLength(50)]
        public string PhoneType { get; set; }

        [StringLength(30)]
        public string PhoneExtention { get; set; }

        [StringLength(20)]
        public string CustomerPhone { get; set; }
    }
}
