namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobOldCustomer")]
    public partial class JobOldCustomer
    {
        public Guid Id { get; set; }

        public Guid JobId { get; set; }

        public Guid CustomerId { get; set; }

        public DateTime DateCreated { get; set; }

        public virtual Customer Customer { get; set; }

        public virtual Job Job { get; set; }
    }
}
