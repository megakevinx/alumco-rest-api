﻿using System.Data.Entity;

namespace AlumcoRestApi.Models
{
    public partial class AlumcoEntityDataModel
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<aspnet_Applications>()
                .HasMany(e => e.aspnet_Membership)
                .WithRequired(e => e.aspnet_Applications)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<aspnet_Applications>()
                .HasMany(e => e.aspnet_Paths)
                .WithRequired(e => e.aspnet_Applications)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<aspnet_Applications>()
                .HasMany(e => e.aspnet_Roles)
                .WithRequired(e => e.aspnet_Applications)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<aspnet_Applications>()
                .HasMany(e => e.aspnet_Users)
                .WithRequired(e => e.aspnet_Applications)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<aspnet_Paths>()
                .HasOptional(e => e.aspnet_PersonalizationAllUsers)
                .WithRequired(e => e.aspnet_Paths);

            modelBuilder.Entity<aspnet_Roles>()
                .HasMany(e => e.aspnet_Users)
                .WithMany(e => e.aspnet_Roles)
                .Map(m => m.ToTable("aspnet_UsersInRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<aspnet_Users>()
                .HasOptional(e => e.aspnet_Membership)
                .WithRequired(e => e.aspnet_Users);

            modelBuilder.Entity<aspnet_Users>()
                .HasOptional(e => e.aspnet_Profile)
                .WithRequired(e => e.aspnet_Users);

            modelBuilder.Entity<aspnet_Users>()
                .HasMany(e => e.Users)
                .WithRequired(e => e.aspnet_Users)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<aspnet_WebEvent_Events>()
                .Property(e => e.EventId)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<aspnet_WebEvent_Events>()
                .Property(e => e.EventSequence)
                .HasPrecision(19, 0);

            modelBuilder.Entity<aspnet_WebEvent_Events>()
                .Property(e => e.EventOccurrence)
                .HasPrecision(19, 0);

            modelBuilder.Entity<Contractor>()
                .HasMany(e => e.ContractorDocuments)
                .WithRequired(e => e.Contractor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Contractor>()
                .HasMany(e => e.JobMaterialLogouts)
                .WithRequired(e => e.Contractor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Customer>()
                .HasMany(e => e.CustomerPhoneNumbers)
                .WithRequired(e => e.Customer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Customer>()
                .HasMany(e => e.Jobs)
                .WithRequired(e => e.Customer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Customer>()
                .HasMany(e => e.JobOldCustomers)
                .WithRequired(e => e.Customer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DistributionList>()
                .HasMany(e => e.DistributionListRecipients)
                .WithRequired(e => e.DistributionList)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Document>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<Document>()
                .Property(e => e.Link)
                .IsUnicode(false);

            modelBuilder.Entity<DocumentType>()
                .HasMany(e => e.JobDocuments)
                .WithRequired(e => e.DocumentType)
                .HasForeignKey(e => e.JobDocumentTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DocumentType>()
                .HasMany(e => e.LeadDocuments)
                .WithRequired(e => e.DocumentType)
                .HasForeignKey(e => e.LeadDocumentTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DocumentType>()
                .HasMany(e => e.ServiceDocuments)
                .WithRequired(e => e.DocumentType)
                .HasForeignKey(e => e.ServiceDocumentTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.Latitude)
                .HasPrecision(9, 6);

            modelBuilder.Entity<Job>()
                .Property(e => e.Longitude)
                .HasPrecision(9, 6);

            modelBuilder.Entity<Job>()
                .HasMany(e => e.GutterWorkOrders)
                .WithRequired(e => e.Job)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
                .HasMany(e => e.JobSigns)
                .WithRequired(e => e.Job)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
                .HasMany(e => e.JobContractors)
                .WithRequired(e => e.Job)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
                .HasMany(e => e.JobDocuments)
                .WithRequired(e => e.Job)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
                .HasMany(e => e.JobFlags)
                .WithRequired(e => e.Job)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
                .HasMany(e => e.JobLogs)
                .WithRequired(e => e.Job)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
                .HasMany(e => e.JobMaterialLogouts)
                .WithRequired(e => e.Job)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
                .HasMany(e => e.JobNotes)
                .WithRequired(e => e.Job)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
                .HasMany(e => e.JobOldCustomers)
                .WithRequired(e => e.Job)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
                .HasMany(e => e.JobPriceAdjustments)
                .WithRequired(e => e.Job)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
                .HasMany(e => e.JobProblems)
                .WithRequired(e => e.Job)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
                .HasMany(e => e.JobRRPEmails)
                .WithRequired(e => e.Job)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
                .HasMany(e => e.Permits)
                .WithRequired(e => e.Job)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
                .HasMany(e => e.ServiceCalls)
                .WithRequired(e => e.Job)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
                .HasMany(e => e.ServiceComments1)
                .WithRequired(e => e.Job)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
                .HasMany(e => e.Shares)
                .WithRequired(e => e.Job)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
                .HasMany(e => e.UserFavoriteJobs)
                .WithRequired(e => e.Job)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
                .HasMany(e => e.Warranties)
                .WithRequired(e => e.Job)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
                .HasMany(e => e.WorkOrders)
                .WithRequired(e => e.Job)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobDocument>()
                .HasMany(e => e.ShareDocuments)
                .WithRequired(e => e.JobDocument)
                .HasForeignKey(e => e.DocumentId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobMaterial>()
                .HasMany(e => e.JobMaterialItems)
                .WithRequired(e => e.JobMaterial)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobMaterialLogout>()
                .HasMany(e => e.JobMaterialItems)
                .WithRequired(e => e.JobMaterialLogout)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobSignStatu>()
                .HasMany(e => e.JobSigns)
                .WithRequired(e => e.JobSignStatu)
                .HasForeignKey(e => e.JobSignStatusId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobStatu>()
                .HasMany(e => e.Jobs)
                .WithRequired(e => e.JobStatu)
                .HasForeignKey(e => e.JobStatusId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lead>()
                .HasMany(e => e.LeadDocuments)
                .WithRequired(e => e.Lead)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lead>()
                .HasMany(e => e.LeadNotes)
                .WithRequired(e => e.Lead)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lead>()
                .HasMany(e => e.LeadProblems)
                .WithRequired(e => e.Lead)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lead>()
                .HasMany(e => e.LeadServices)
                .WithRequired(e => e.Lead)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LeadPhoneNumber>()
                .Property(e => e.PhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<LeadSource>()
                .HasMany(e => e.Leads)
                .WithOptional(e => e.LeadSource)
                .HasForeignKey(e => e.SourceId);

            modelBuilder.Entity<LeadStatu>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<LeadStatu>()
                .HasMany(e => e.Leads)
                .WithRequired(e => e.LeadStatu)
                .HasForeignKey(e => e.StatusId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Material>()
                .Property(e => e.UnitType)
                .IsUnicode(false);

            modelBuilder.Entity<Material>()
                .HasMany(e => e.WorkOrderMaterials)
                .WithRequired(e => e.Material)
                .HasForeignKey(e => e.MaterialTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Permit>()
                .HasMany(e => e.PermitDocuments)
                .WithRequired(e => e.Permit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PermitStatu>()
                .HasMany(e => e.Permits)
                .WithOptional(e => e.PermitStatu)
                .HasForeignKey(e => e.PermitStatusId);

            modelBuilder.Entity<PhoneNumberType>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<PhoneNumberType>()
                .HasMany(e => e.CustomerPhoneNumbers)
                .WithRequired(e => e.PhoneNumberType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PhoneNumberType>()
                .HasMany(e => e.LeadPhoneNumbers)
                .WithRequired(e => e.PhoneNumberType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Service>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Service>()
                .HasMany(e => e.LeadServices)
                .WithRequired(e => e.Service)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Service>()
                .HasMany(e => e.VendorServices)
                .WithRequired(e => e.Service)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ServiceCall>()
                .HasMany(e => e.ServiceCallTeches)
                .WithRequired(e => e.ServiceCall)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ServiceCall>()
                .HasMany(e => e.ServiceCallNoteChecklistItems)
                .WithRequired(e => e.ServiceCall)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ServiceCall>()
                .HasMany(e => e.ServiceDocuments)
                .WithRequired(e => e.ServiceCall)
                .HasForeignKey(e => e.ServiceId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ServiceCall>()
                .HasMany(e => e.ServiceLogs)
                .WithRequired(e => e.ServiceCall)
                .HasForeignKey(e => e.ServiceId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ServiceCall>()
                .HasMany(e => e.ServiceProblems)
                .WithRequired(e => e.ServiceCall)
                .HasForeignKey(e => e.ServiceId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ServiceCall>()
                .HasMany(e => e.ServiceTechNotes)
                .WithRequired(e => e.ServiceCall)
                .HasForeignKey(e => e.ServiceId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ServiceCallNoteChecklistItemType>()
                .HasMany(e => e.ServiceCallNoteChecklistItems)
                .WithRequired(e => e.ServiceCallNoteChecklistItemType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Share>()
                .HasMany(e => e.ShareDocuments)
                .WithRequired(e => e.Share)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Supplier>()
                .HasMany(e => e.StockInventories)
                .WithRequired(e => e.Supplier)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Term>()
                .HasMany(e => e.Jobs)
                .WithRequired(e => e.Term)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TypeOfService>()
                .HasMany(e => e.ServiceCalls)
                .WithRequired(e => e.TypeOfService)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Jobs)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.SalesPersonId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Jobs1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.ProjectManagerId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Jobs2)
                .WithOptional(e => e.User2)
                .HasForeignKey(e => e.SalesPersonId2);

            modelBuilder.Entity<User>()
                .HasMany(e => e.JobContractors)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.ServiceTechId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.JobSigns)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.TechId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Leads)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.SalesPersonId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Permits)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.ProjectManagerId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.ServiceCalls)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.ServiceTechId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.ServiceCallTeches)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.ServiceTechId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.UserFavoriteJobs)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Vendor>()
                .HasMany(e => e.VendorServices)
                .WithRequired(e => e.Vendor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Warranty>()
                .HasMany(e => e.WarrantyValuesXRefs)
                .WithRequired(e => e.Warranty)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<WarrantyDropDownList>()
                .HasMany(e => e.WarrantyDropDownXRefs)
                .WithRequired(e => e.WarrantyDropDownList)
                .HasForeignKey(e => e.WarrantyDropDownId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<WarrantyDropDownList>()
                .HasMany(e => e.WarrantyValuesXRefs)
                .WithRequired(e => e.WarrantyDropDownList)
                .HasForeignKey(e => e.WarrantyDropDownValueId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<WarrantyItem>()
                .HasMany(e => e.Warranties)
                .WithRequired(e => e.WarrantyItem)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<WarrantyItem>()
                .HasMany(e => e.WarrantyDropDownXRefs)
                .WithRequired(e => e.WarrantyItem)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<WorkOrder>()
                .HasMany(e => e.WorkOrderMaterials)
                .WithRequired(e => e.WorkOrder)
                .WillCascadeOnDelete(false);

            // Custom relationships
            modelBuilder.Entity<Lead>()
                .HasMany(e => e.LeadPhoneNumbers)
                .WithRequired(e => e.Lead)
                .WillCascadeOnDelete(false);
        }
    }
}