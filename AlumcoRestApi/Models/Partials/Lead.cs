using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AlumcoRestApi.Models
{
    public partial class Lead
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Lead()
        {
            Jobs = new HashSet<Job>();
            LeadDocuments = new HashSet<LeadDocument>();
            LeadNotes = new HashSet<LeadNote>();
            LeadProblems = new HashSet<LeadProblem>();
            LeadServices = new HashSet<LeadService>();
            LeadPhoneNumbers = new HashSet<LeadPhoneNumber>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LeadPhoneNumber> LeadPhoneNumbers { get; set; }
    }
}
