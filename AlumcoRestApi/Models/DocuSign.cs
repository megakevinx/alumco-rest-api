namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DocuSign")]
    public partial class DocuSign
    {
        [Key]
        [Column(Order = 0)]
        public Guid Id { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid JobId { get; set; }

        [StringLength(300)]
        public string Note { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime DateCreated { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(50)]
        public string CreatedBy { get; set; }
    }
}
