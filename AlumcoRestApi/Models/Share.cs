namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Share")]
    public partial class Share
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Share()
        {
            ShareDocuments = new HashSet<ShareDocument>();
        }

        public Guid Id { get; set; }

        public Guid JobId { get; set; }

        public DateTime SharedDate { get; set; }

        [Required]
        [StringLength(256)]
        public string SharedBy { get; set; }

        public virtual Job Job { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ShareDocument> ShareDocuments { get; set; }
    }
}
