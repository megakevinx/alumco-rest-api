namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("WarrantyDropDownXRef")]
    public partial class WarrantyDropDownXRef
    {
        public Guid Id { get; set; }

        public Guid WarrantyItemId { get; set; }

        public Guid WarrantyDropDownId { get; set; }

        [StringLength(255)]
        public string DropDownName { get; set; }

        [StringLength(255)]
        public string WarrantyLetterText { get; set; }

        public virtual WarrantyDropDownList WarrantyDropDownList { get; set; }

        public virtual WarrantyItem WarrantyItem { get; set; }
    }
}
