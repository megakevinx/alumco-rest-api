namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("WorkOrderMaterial")]
    public partial class WorkOrderMaterial
    {
        public Guid Id { get; set; }

        public int Quantity { get; set; }

        [Required]
        [StringLength(20)]
        public string Color { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public string Additional { get; set; }

        [Required]
        [StringLength(250)]
        public string Returned { get; set; }

        public Guid MaterialTypeId { get; set; }

        public Guid WorkOrderId { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }

        public virtual Material Material { get; set; }

        public virtual WorkOrder WorkOrder { get; set; }
    }
}
