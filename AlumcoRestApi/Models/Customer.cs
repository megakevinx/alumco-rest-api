namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Customer")]
    public partial class Customer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Customer()
        {
            CustomerPhoneNumbers = new HashSet<CustomerPhoneNumber>();
            Jobs = new HashSet<Job>();
            JobOldCustomers = new HashSet<JobOldCustomer>();
        }

        public Guid Id { get; set; }

        [Required]
        [StringLength(20)]
        public string TitleOfCourtesy1 { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstName1 { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName1 { get; set; }

        [Required]
        [StringLength(20)]
        public string TitleOfCourtesy2 { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstName2 { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName2 { get; set; }

        [Required]
        [StringLength(500)]
        public string Greeting { get; set; }

        [Required]
        [StringLength(50)]
        public string CompanyName { get; set; }

        [Required]
        [StringLength(256)]
        public string Email { get; set; }

        [Required]
        [StringLength(256)]
        public string AlternateEmail { get; set; }

        [Required]
        public string PhoneComments { get; set; }

        [Required]
        [StringLength(55)]
        public string BillingAddress { get; set; }

        [Required]
        [StringLength(20)]
        public string BillingCity { get; set; }

        [Required]
        [StringLength(2)]
        public string BillingState { get; set; }

        [Required]
        [StringLength(5)]
        public string BillingZip { get; set; }

        [Required]
        [StringLength(100)]
        public string AddressPreview { get; set; }

        public bool Referral { get; set; }

        public bool Services { get; set; }

        public bool Surveys { get; set; }

        public bool GuildQuality { get; set; }

        public bool RadioCommercial { get; set; }

        public bool TVCommercial { get; set; }

        public bool Testimonial { get; set; }

        [Required]
        public string Notes { get; set; }

        public int? OldJobId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerPhoneNumber> CustomerPhoneNumbers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Job> Jobs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobOldCustomer> JobOldCustomers { get; set; }
    }
}
