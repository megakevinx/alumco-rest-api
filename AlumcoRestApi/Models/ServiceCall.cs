namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ServiceCall")]
    public partial class ServiceCall
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ServiceCall()
        {
            JobProblems = new HashSet<JobProblem>();
            ServiceCallTeches = new HashSet<ServiceCallTech>();
            ServiceCallNoteChecklistItems = new HashSet<ServiceCallNoteChecklistItem>();
            ServiceDocuments = new HashSet<ServiceDocument>();
            ServiceLogs = new HashSet<ServiceLog>();
            ServiceProblems = new HashSet<ServiceProblem>();
            ServiceTechNotes = new HashSet<ServiceTechNote>();
        }

        public Guid Id { get; set; }

        public Guid JobId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ServiceCallId { get; set; }

        public DateTime CallDate { get; set; }

        [Required]
        public string Problem { get; set; }

        public Guid? ServiceTechId { get; set; }

        public DateTime? AppointmentDate { get; set; }

        public bool Completed { get; set; }

        public DateTime? DateCompleted { get; set; }

        public DateTime? CourtesyCallDate { get; set; }

        [StringLength(20)]
        public string CourtesyCallTime { get; set; }

        public Guid TypeOfServiceId { get; set; }

        [Required]
        public string TechnicianNotes { get; set; }

        public DateTime? CheckInDate { get; set; }

        public DateTime? CheckOutDate { get; set; }

        [StringLength(100)]
        public string CreateUser { get; set; }

        public DateTime? CreateDate { get; set; }

        [StringLength(100)]
        public string UpdateUser { get; set; }

        public DateTime? UpdateDate { get; set; }

        public Guid? ContractorId { get; set; }

        [StringLength(100)]
        public string CourtesyUpdateUser { get; set; }

        public DateTime? CourtesyUpdateDate { get; set; }

        [StringLength(100)]
        public string CompletedUser { get; set; }

        public DateTime? PartDate { get; set; }

        public DateTime? PartDateCreated { get; set; }

        [StringLength(100)]
        public string PartDateUser { get; set; }

        [StringLength(1000)]
        public string Email { get; set; }

        public virtual Contractor Contractor { get; set; }

        public virtual Job Job { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobProblem> JobProblems { get; set; }

        public virtual TypeOfService TypeOfService { get; set; }

        public virtual User User { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ServiceCallTech> ServiceCallTeches { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ServiceCallNoteChecklistItem> ServiceCallNoteChecklistItems { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ServiceDocument> ServiceDocuments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ServiceLog> ServiceLogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ServiceProblem> ServiceProblems { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ServiceTechNote> ServiceTechNotes { get; set; }
    }
}
