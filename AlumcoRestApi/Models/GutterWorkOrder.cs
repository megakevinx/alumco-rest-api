namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GutterWorkOrder")]
    public partial class GutterWorkOrder
    {
        public Guid Id { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        [StringLength(50)]
        public string Color { get; set; }

        [Required]
        [StringLength(50)]
        public string ResidentialGutter { get; set; }

        [Required]
        [StringLength(50)]
        public string CommercialGutter { get; set; }

        [Required]
        [StringLength(50)]
        public string ResidentialDownspout { get; set; }

        [Required]
        [StringLength(50)]
        public string CommercialDownspout { get; set; }

        [Required]
        [StringLength(50)]
        public string Outlets { get; set; }

        [Required]
        [StringLength(50)]
        public string Miters { get; set; }

        [Required]
        [StringLength(50)]
        public string Elbows { get; set; }

        [Required]
        [StringLength(50)]
        public string HiddenHangers { get; set; }

        [Required]
        [StringLength(50)]
        public string ValleyShields { get; set; }

        [Required]
        [StringLength(50)]
        public string TubesOfGutterSeal { get; set; }

        [Required]
        [StringLength(50)]
        public string GuttersRemoved { get; set; }

        [Required]
        [StringLength(50)]
        public string GutterHelmets { get; set; }

        public int JobLengthHours { get; set; }

        public int JobLengthMinutes { get; set; }

        [Required]
        public string OtherWorkDone { get; set; }

        public bool JobSignPosted { get; set; }

        public bool JobSignPickedUp { get; set; }

        public bool CustomerSeen { get; set; }

        [Required]
        public string Comments { get; set; }

        public Guid JobId { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }

        public virtual Job Job { get; set; }
    }
}
