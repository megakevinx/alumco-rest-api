namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LeadProblem")]
    public partial class LeadProblem
    {
        public Guid Id { get; set; }

        public Guid LeadId { get; set; }

        [Required]
        public string Problem { get; set; }

        public bool CourtesyCall { get; set; }

        public DateTime DateCreated { get; set; }

        [Required]
        [StringLength(256)]
        public string CreatedBy { get; set; }

        public virtual Lead Lead { get; set; }
    }
}
