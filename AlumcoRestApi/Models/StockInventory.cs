namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StockInventory")]
    public partial class StockInventory
    {
        public Guid Id { get; set; }

        public Guid SupplierID { get; set; }

        public int Quantity { get; set; }

        public Guid SalesPersonID { get; set; }

        public DateTime Date { get; set; }

        [Column(TypeName = "ntext")]
        [Required]
        public string Description { get; set; }

        public virtual Supplier Supplier { get; set; }
    }
}
