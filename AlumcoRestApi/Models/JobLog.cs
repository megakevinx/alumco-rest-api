namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobLog")]
    public partial class JobLog
    {
        public Guid Id { get; set; }

        public Guid JobId { get; set; }

        [Required]
        [StringLength(20)]
        public string CheckType { get; set; }

        public bool IsManual { get; set; }

        public DateTime DateCreated { get; set; }

        [Required]
        [StringLength(256)]
        public string CreatedBy { get; set; }

        public virtual Job Job { get; set; }
    }
}
