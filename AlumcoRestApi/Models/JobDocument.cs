namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobDocument")]
    public partial class JobDocument
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public JobDocument()
        {
            ShareDocuments = new HashSet<ShareDocument>();
        }

        public Guid Id { get; set; }

        public Guid JobId { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        public Guid JobDocumentTypeId { get; set; }

        [Required]
        [StringLength(256)]
        public string Path { get; set; }

        public DateTime DateCreated { get; set; }

        [Required]
        [StringLength(256)]
        public string CreatedBy { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        [StringLength(100)]
        public string FileName { get; set; }

        public virtual DocumentType DocumentType { get; set; }

        public virtual Job Job { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ShareDocument> ShareDocuments { get; set; }
    }
}
