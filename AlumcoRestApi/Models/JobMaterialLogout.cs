namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobMaterialLogout")]
    public partial class JobMaterialLogout
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public JobMaterialLogout()
        {
            JobMaterialItems = new HashSet<JobMaterialItem>();
        }

        public Guid Id { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public Guid JobId { get; set; }

        public DateTime OrderDate { get; set; }

        public Guid ContractorId { get; set; }

        public virtual Contractor Contractor { get; set; }

        public virtual Job Job { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobMaterialItem> JobMaterialItems { get; set; }
    }
}
