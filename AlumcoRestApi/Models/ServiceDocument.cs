namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ServiceDocument")]
    public partial class ServiceDocument
    {
        public Guid Id { get; set; }

        public Guid ServiceId { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        public Guid ServiceDocumentTypeId { get; set; }

        [Required]
        [StringLength(256)]
        public string Path { get; set; }

        public DateTime DateCreated { get; set; }

        [Required]
        [StringLength(256)]
        public string CreatedBy { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        [StringLength(100)]
        public string FileName { get; set; }

        public virtual DocumentType DocumentType { get; set; }

        public virtual ServiceCall ServiceCall { get; set; }
    }
}
