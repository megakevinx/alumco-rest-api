namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobSign")]
    public partial class JobSign
    {
        public Guid Id { get; set; }

        public Guid JobId { get; set; }

        public Guid TechId { get; set; }

        public Guid JobSignStatusId { get; set; }

        public DateTime DateAssigned { get; set; }

        public bool Verified { get; set; }

        public DateTime? DateReturned { get; set; }

        [StringLength(256)]
        public string ReturnedBy { get; set; }

        public virtual Job Job { get; set; }

        public virtual User User { get; set; }

        public virtual JobSignStatu JobSignStatu { get; set; }
    }
}
