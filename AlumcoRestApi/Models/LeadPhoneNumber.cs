namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LeadPhoneNumber")]
    public partial class LeadPhoneNumber
    {
        //public Guid Id { get; set; }

        public Guid LeadID { get; set; }

        public Guid PhoneNumberTypeId { get; set; }

        [Required]
        [StringLength(40)]
        public string PhoneNumber { get; set; }

        [Required]
        [StringLength(20)]
        public string Extension { get; set; }

        public bool Preferred { get; set; }

        public virtual PhoneNumberType PhoneNumberType { get; set; }
    }
}
