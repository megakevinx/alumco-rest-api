namespace AlumcoRestApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ServiceProblem")]
    public partial class ServiceProblem
    {
        public Guid Id { get; set; }

        public Guid ServiceId { get; set; }

        [Required]
        public string Problem { get; set; }

        public DateTime DateCreated { get; set; }

        [Required]
        [StringLength(256)]
        public string CreatedBy { get; set; }

        public virtual ServiceCall ServiceCall { get; set; }
    }
}
