﻿using AlumcoRestApi.Helpers;
using AlumcoRestApi.Models;
using AlumcoRestApi.ResourceModels;
using AlumcoRestApi.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Http;
using System.Web.Http.Description;

namespace AlumcoRestApi.Controllers
{
    public class LeadsController : ApiController
    {
        //private AlumcoEntityDataModel db = new AlumcoEntityDataModel();
        private Services.LeadService service = new Services.LeadService();
        private TemplatedEmailDeliveryHelper emailHelper =
            new TemplatedEmailDeliveryHelper(new DistributionListService());

        // GET: api/Leads
        public IEnumerable<LeadResourceModel> GetLeads([FromUri] int page = 0, [FromUri] int count = 10)
        {
            return service.GetAll(page, count);
        }

        // GET: api/Leads/5
        [ResponseType(typeof(LeadResourceModel))]
        public IHttpActionResult GetLead(Guid id)
        {
            LeadResourceModel lead = service.GetById(id);

            if (lead == null)
            {
                return NotFound();
            }

            return Ok(lead);
        }

        //// PUT: api/Leads/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutLead(Guid id, Lead lead)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != lead.Id)
        //    {
        //        return BadRequest();
        //    }

        //    //db.Entry(lead).State = EntityState.Modified;

        //    try
        //    {
        //        //db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        //if (!LeadExists(id))
        //        //{
        //        //    return NotFound();
        //        //}
        //        //else
        //        //{
        //        //    throw;
        //        //}
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        // POST: api/Leads
        [ResponseType(typeof(Lead))]
        public IHttpActionResult PostLead(LeadResourceModel lead)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var createdLead = service.Create(lead);

            emailHelper.SendToDistributionList(
                "~/Views/Email/NewLeadToAdmin.cshtml", createdLead,
                ConfigurationManager.AppSettings["NewLeadFromEmailAddress"],
                ConfigurationManager.AppSettings["NewLeadToDistributionList"],
                ConfigurationManager.AppSettings["NewLeadEmailSubject"]
            );

            emailHelper.SendToSingleRecipient(
                "~/Views/Email/NewLeadToCustomer.cshtml",
                ConfigurationManager.AppSettings["NewLeadFromEmailAddress"],
                createdLead.Email,
                ConfigurationManager.AppSettings["NewLeadEmailSubject"]
            );

            return CreatedAtRoute("DefaultApi", new { id = createdLead.Id }, createdLead);
        }

        // DELETE: api/Leads/5
        //[ResponseType(typeof(Lead))]
        //public IHttpActionResult DeleteLead(Guid id)
        //{
        //    Lead lead = db.Leads.Find(id);
        //    if (lead == null)
        //    {
        //        return NotFound();
        //    }

        //    db.Leads.Remove(lead);
        //    db.SaveChanges();

        //    return Ok(lead);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                service.Dispose();
            }
            base.Dispose(disposing);
        }

        //private bool LeadExists(Guid id)
        //{
        //    return db.Leads.Count(e => e.Id == id) > 0;
        //}
    }
}