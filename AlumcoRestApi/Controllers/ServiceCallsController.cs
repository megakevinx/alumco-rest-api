﻿using AlumcoRestApi.Helpers;
using AlumcoRestApi.Models;
using AlumcoRestApi.ResourceModels;
using AlumcoRestApi.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Http;
using System.Web.Http.Description;

namespace AlumcoRestApi.Controllers
{
    public class ServiceCallsController : ApiController
    {
        private ServiceCallService service = new ServiceCallService();

        // GET: api/Leads
        public IEnumerable<ServiceCallResourceModel> GetServiceCalls([FromUri] string date)
        {
            return service.GetAllForDate(date);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                service.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}